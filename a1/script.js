/*
https://codeshare.io/dwBQjZ

Quiz:

1) How do you create arrays in JS?

2) How do you access the first character of an array?

3) How do you access the last character of an array?

4) What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.

5) What array method loops over all elements of an array, performing a user-defined function on each iteration?

6) What array method creates a new array with elements obtained from a user-defined function?

7) What array method checks if all its elements satisfy a given condition?

8) What array method checks if at least one of its elements satisfies a given condition?

9) True or False: array.splice() modifies a copy of the array, leaving the original unchanged.

10) True or False: array.slice() copies elements from original array and returns them as a new array.
*/

/*1*/
let ajArray = ["a", "b", "c", "d"];

/*2*/
//ajArray[0];
let firstCharacter = ajArray[0];

/*3*/
//ajArray[ajArray.length - 1];
let lastCharacter = ajArray[ajArray.length - 1];

/*4*/
//indexOf("b")
let index = ajArray.indexOf("b");

/*5*/
//forEach()
ajArray.forEach(function(element) {
	console.log(element)
})

/*6*/
//map()
let newAjArray = ajArray.map(function(element) {
	return element * 2;
})

/*7*/
//every()
let ajNumArray = [1,2,3,4,5];
let allGreaterThanZero = ajNumArray.every(function(elem) {
	return elem > 0;
})

/*8*/
//some()
let evenNum = ajNumArray.some(function(param) {
	return param % 2 === 0;
})

/*9*/
//false

/*10*/
//true


/*============================================================*/

/*
Function Coding
*/
const students = ["John", "Joe", "Jane", "Jessie"];
/*
1) Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.
*/

function addToEnd(array, element) {
	if(typeof element !== 'string')
	{
		return "can only add string to an array";
	}

	array.push(element);
	return array;
};
let updateAddToEndStudents = addToEnd(students, "Ryan");
let validate = addToEnd(students, 49);
console.log(updateAddToEndStudents);
console.log(validate);
/*
Output:

addToEnd(students, "Ryan"); //["John", "Joe", "Jane", "Jessie", "Ryan"]
addToEnd(students, 045); //"error - can only add strings to an array"
*/




/*
2) Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.
*/
/*2*/
function addToStart(array, element) {
	if(typeof element !== 'string')
	{
		return "add only string";
	}

	array.unshift(element);
	return array;
}

let updateAddToStartStudents = addToStart(students, 'Tess');
let validate2 = (students, 45);
console.log(updateAddToStartStudents);
console.log(validate2);




/*
3) Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.
*/
function elementChecker(arr, element)
{
	if (arr.length === 0)
	{
		return "array is empty";
	}

	return arr.includes(element);
}
//test input
let myChecker = elementChecker(students, "Jane"); //true
//validation check
let myChecketValidate = elementChecker([], "Jane"); //"error - passed in array is empty"
console.log(myChecker);
console.log(myChecketValidate);




/*
4) Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:

if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
if every element in the array ends in the passed in character, return true. Otherwise return false.

Use the students array and the character "e" as arguments when testing.
*/
function checkAllStingsEnding(students, character)
{
	if(students.length === 0)
	{
		return "error - array must NOT be empty";
	}
	if(!students.every(element => typeof element === 'string'))
	{
		return "error - all array elements must be strings";
	}
	if(typeof character !== 'string')
	{
		return "error - 2nd argument must be of data type string"
	}
	if(character.length !== 1)
	{
		return "error - 2nd argument must be a single character";
	}

	return students.every(element => element.endsWith(character));
}
let allStringEndWithE1 = checkAllStingsEnding(students, "e");
let allStringEndWithE2 = checkAllStingsEnding([], "e");
let allStringEndWithE3 = checkAllStingsEnding(["Jane", 02], "e");
let allStringEndWithE4 = checkAllStingsEnding(students, 4);
let allStringEndWithE5 = checkAllStingsEnding(students, "el");
console.log(allStringEndWithE1);
console.log(allStringEndWithE2);
console.log(allStringEndWithE3);
console.log(allStringEndWithE4);
console.log(allStringEndWithE5);
/*
//test input
checkAllStringsEnding(students, "e"); //false
//validation checks
checkAllStringsEnding([], "e"); //"error - array must NOT be empty"
checkAllStringsEnding(["Jane", 02], "e"); //"error - all array elements must be strings"
checkAllStringsEnding(students, 4); //"error - 2nd argument must be of data type string"
checkAllStringsEnding(students, "el"); //"error - 2nd argument must be a single character"
*/




/*
5) Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.
*/
function stringLengthSorter(students) {
  if (!students.every(element => typeof element === 'string')) {
    return "error - all array elements must be strings";
  }
  
  students.sort((a, b) => a.length - b.length);
  return students;
}
let sortedArray = stringLengthSorter(students);
let sortedArrayValidate = stringLengthSorter([037, "John", 039, "Jane"]);
console.log(sortedArray);
console.log(sortedArrayValidate);
/*
//test input
stringLengthSorter(students); //["Joe", "Tess", "John", "Jane", "Ryan", "Jessie"]
//validation check
stringLengthSorter([037, "John", 039, "Jane"]); //"error - all array elements must be strings"
*/




/*
6) Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:

if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
return the number of elements in the array that start with the character argument, must be case-insensitive

Use the students array and the character "J" as arguments when testing.
*/
function startsWithCounter(students, character)
{
  if (students.length === 0)
  {
    return "error - array must NOT be empty";
  }
  
  if (!students.every(element => typeof element === 'string'))
  {
    return "error - all array elements must be strings";
  }
  
  if (typeof character !== 'string')
  {
    return "error - 2nd argument must be of data type string";
  }
  
  if (character.length !== 1)
  {
    return "error - 2nd argument must be a single character";
  }
  
  var count = 0;
  var lowercaseChar = character.toLowerCase();
  
  for (var i = 0; i < students.length; i++)
  {
    var lowercaseElement = students[i].toLowerCase();
    
    if (lowercaseElement.startsWith(lowercaseChar))
    {
      count++;
    }
  }
  
  return count;
};
var startsWithJCount = startsWithCounter(students, "j");
console.log(startsWithJCount);

/*
//test input
startsWithCounter(students, "j"); //4
*/




/*
7) Create a function named likeFinder that will take in an array of strings and a string to be searched for. The function will do the ff:

if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
return a new array containing all elements of the array argument that contain the string argument in it, must be case-insensitive

Use the students array and the string "jo" as arguments when testing.
*/
function likeFinder(arr, searchString)
{
  if (arr.length === 0)
  {
    return "error - array must NOT be empty";
  }
  
  if (!arr.every(element => typeof element === 'string'))
  {
    return "error - all array elements must be strings";
  }
  
  if (typeof searchString !== 'string')
  {
    return "error - 2nd argument must be of data type string";
  }
  
  var lowercaseSearchString = searchString.toLowerCase();
  
  var matchingElements = arr.filter(element =>
  {
    var lowercaseElement = element.toLowerCase();
    return lowercaseElement.includes(lowercaseSearchString);
  });
  
  return matchingElements;
}

var matchingStudents = likeFinder(students, "jo");
console.log(matchingStudents);

/*
//test input
likeFinder(students, "jo"); //["Joe", "John"]
*/




/*
8) Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.
*/
function randomPicker(arr)
{
  if (arr.length === 0)
  {
    return "error - array must NOT be empty";
  }
  
  var randomIndex = Math.floor(Math.random() * arr.length);
  return arr[randomIndex];
}

var randomStudent = randomPicker(students);

console.log(randomStudent); // Output: (a random element from the students array)

/*
//test input
randomPicker(students); //"Ryan"
randomPicker(students); //"John"
randomPicker(students); //"Jessie"
*/
